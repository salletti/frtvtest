<?php

namespace ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use ApplicationBundle\Form\ArticleType;
use ApplicationBundle\Entity\Article;


class ApiController extends FOSRestController
{

    /**
     * @return array
     * @Rest\Get("/articles/", defaults={})
     * @ApiDoc(
     *   resource = true,
     *   description = "Return the overall Article List",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *   }
     * )
     *
     *
     */
    public function articlesAction()
    {
        $articleManager = $this->get('article.manager');
        $articles = $articleManager->getList();

        $view = View::create();
        $view->setData($this->serializeEntity($articles))->setStatusCode(200)
            ->setTemplate("ApiBundle:Default:output.json.twig")
            ->setTemplateVar('data');

        return $this->handleView($view);
    }

    /**
     *
     * @return View
     * @Rest\Get("/article/{slug}", defaults={})
     * @ApiDoc(
     *   resource = true,
     *   description = "Return the overall Article List",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the article is not found"
     *   }
     * )
     *
     *
     */
    public function articleAction($slug)
    {
        $articleManager = $this->get('article.manager');
        $article = $articleManager->getBySlug($slug);

        $view = View::create();
        if(!$article){
            $view->setStatusCode(400);
            $view->setData('not found')
            ->setTemplate("ApiBundle:Default:output.json.twig")
            ->setTemplateVar('data');
            return $this->handleView($view);
        }


        $view->setData($this->serializeEntity($article))->setStatusCode(200)
        ->setTemplate("ApiBundle:Default:output.json.twig")
        ->setTemplateVar('data');
        return $this->handleView($view);
    }

    /**
     * @return array
     * @Rest\Post("/articles")
     * @Rest\View("ApiBundle:Default:output.json.twig")
     * @ApiDoc(
     *   resource = true,
     *   description = "Return the overall Article List",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the article is not found"
     *   }
     * )
     *
     *
     */
    public function createAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        $view = View::create();
        if ($form->isValid()) {
            $articleManager = $this->get('article.manager');
            $articleManager->insert($article);

            $view->setData(array('message' => 'article created'))->setStatusCode(200);
        }

        $view->setData($form->getErrors())->setStatusCode(400);

        $view->setTemplate("ApiBundle:Default:output.json.twig");
        $view->setTemplateVar('data');

        return $this->handleView($view);

    }

    protected function serializeEntity($entity, $format = 'json')
    {
        $serializer = $this->container->get('jms_serializer');
        return $serializer->serialize($entity, $format);
    }


}