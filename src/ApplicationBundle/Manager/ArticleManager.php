<?php

namespace ApplicationBundle\Manager;

use Doctrine\ORM\EntityManager;

class ArticleManager
{
    protected $em;
    protected $repository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('ApplicationBundle:Article');
    }

    /**
     * Get article by Id
     *
     * @param $id
     * @return null|object
     */
    public function getById($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Get article by slug
     *
     * @param $slug
     * @return null|object
     */
    public function getBySlug($slug)
    {
        return $this->repository->findOneBy(array('slug' => $slug));
    }

    public function getList()
    {
        return $this->repository->findAll();
    }

    public function insert($article)
    {
        return $this->persistAndFlush($article);
    }

    public function edit()
    {

    }

    protected function persistAndFlush($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
        $this->em->refresh($entity);

        return $entity->getId();
    }

}