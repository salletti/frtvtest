<?php

namespace ApplicationBundle\Controller;

use ApplicationBundle\Entity\Article;
use ApplicationBundle\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ArticleController extends Controller
{
    public function createAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $articleManager = $this->get('article.manager');
            $articleManager->insert($article);
        }

        return $this->render('ApplicationBundle:Default:create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function getAction($slug)
    {
        $articleManager = $this->get('article.manager');
        $article = $articleManager->getBySlug($slug);

        if(!$article){
            throw new HttpException(400, 'page not found');
        }

        return $this->render('ApplicationBundle:Default:single.html.twig', array(
            'article' => $article,
        ));
    }

    public function listAction()
    {
        $articleManager = $this->get('article.manager');
        $articles = $articleManager->getList();

        return $this->render('ApplicationBundle:Default:list.html.twig', array(
            'articles' => $articles,
        ));
    }
}
